import graphene
from graphene_django import DjangoObjectType
from .models import Link, Writer, Vote
from hackernews.users.schema import UserType
from django.db.models import Q


class LinkType(DjangoObjectType):
    class Meta:
        model = Link


class CreateVote(graphene.Mutation):
    user = graphene.Field(UserType)
    link = graphene.Field(LinkType)

    class Arguments:
        link_id = graphene.Int()

    def mutate(self, info, link_id):
        user = info.context.user
        if user.is_anonymous:
            raise Exception("must login in first !")
        link = Link.objects.filter(id=link_id).first()
        if not link:
            raise Exception("invalid link")
        Vote.objects.create(user=user, link=link)
        return CreateVote(user=user, link=link)


class WriterType(DjangoObjectType):
    class Meta:
        model = Writer


class VoteType(DjangoObjectType):
    class Meta:
        model = Vote


class CreateLink(graphene.Mutation):
    id = graphene.Int()
    url = graphene.String()
    description = graphene.String()
    posted_by = graphene.Field(UserType)

    class Arguments:
        url = graphene.String()
        description = graphene.String()

    def mutate(self, info, url, description):
        user = info.context.user or None

        link = Link(description=description, url=url, posted_by=user)
        link.save()

        return CreateLink(id=link.id, url=link.url, description=link.description, posted_by=link.posted_by)


class Query(graphene.ObjectType):
    links = graphene.List(LinkType, search=graphene.String(), first=graphene.Int(), skip=graphene.Int())
    writers = graphene.List(WriterType)
    votes = graphene.List(VoteType)

    def resolve_writers(self, info, **kwargs):
        return Writer.objects.all()

    def resolve_links(self, info, search=None, first=None, skip=None, **kwargs):
        result = Link.objects.all()
        if search:
            filter = (Q(description__icontains=search) | Q(url__icontains=search))
            result = Link.objects.filter(filter)
        if skip:
            result = result[skip:]

        if first:
            result = result[:first]

        return result

    def resolve_votes(self, info, **kwargs):
        return Vote.objects.all()


class Mutation(graphene.ObjectType):
    create_link = CreateLink.Field()
    create_void = CreateVote.Field()
