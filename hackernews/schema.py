import graphene
import links.schema
from .users.schema import Mutation as UserMutation
from .users.schema import Query as UserQuery
import graphql_jwt


class Query(links.schema.Query, UserQuery, graphene.ObjectType):
    pass


class Mutation(links.schema.Mutation, UserMutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
